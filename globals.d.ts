// Directly copied from https://github.com/renovatebot/renovate/blob/39.31.0/lib/globals.d.ts
declare interface Error {
  validationSource?: string;

  validationError?: string;
  validationMessage?: string;
}
