# Updating vendored Renovate code into `renovate-graph`

Within `renovate-graph`, we vendor a number of pieces of code that come from the upstream `renovate` project.

These are performed using pnpm's `pnpm patch` functionality, and allows patching the internal Renovate codebase more easily.

Patched code will be wrapped with a `<modified>...</modified>` comment, to more verbosely indicate that this is where we have introduced custom functionality.

Updating this patch will usually only be required when there are conflicts, or there is a report of a bug occurring.

## Major version updates

The steps for upgrading to a new major version of Renovate is similar to a minor, with a few additional steps:

- Perform any required Node version bumps
  - As noted in [v38 release notes](https://docs.renovatebot.com/release-notes-for-major-versions/#version-38) the minor bumping of Renovate's Node dependency will only occur on a major version bump of Renovate
  - Changes may include CI, Docker configuration, the `engines` in the `package.json` and `tsconfig`
- Check Renovate's [release notes for major versions](https://docs.renovatebot.com/release-notes-for-major-versions/) for anything
- Run a scan before/after the upgrade of a given repository, and if there are significant differences (i.e. it looks like behaviour changes re `depName`, `packageName`, etc), please:
  - document behaviour changes discovered in the Merge Request body
  - raise it upstream with [dependency-management-data](https://dmd.tanna.dev), as a heavy consumer
