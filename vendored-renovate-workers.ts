/**
 * A set of functions that are vendored from Renovate's lib/workers/global/* and lib/workers/repository/* files.
 */
/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { AllConfig, RenovateConfig } from 'renovate/dist/config/types'
import { resolveGlobalExtends, validatePresets } from 'renovate/dist/workers/global/index'
import { globalInitialize } from 'renovate/dist/workers/global/initialize'
import { parseConfigs } from 'renovate/dist/workers/global/config/parse'
import { mergeChildConfig } from 'renovate/dist/config/utils'
import { GlobalConfig } from 'renovate/dist/config/global'
import { applySecretsToConfig, validateConfigSecrets } from 'renovate/dist/config/secrets'
import { initRepo } from 'renovate/dist/workers/repository/init/index'
import { extractDependencies } from 'renovate/dist/workers/repository/process/index'
import { ExtractResult } from 'renovate/dist/workers/repository/process/extract-update'
import { removeAbilityToDisableRenovateGlobally, removeAbilityToDisableRenovateInLocalRepoConfig } from './index'
import { logger } from './logger'
import * as pkg from 'renovate/package.json'
import semver from 'semver'
import { setGlobalLogLevelRemaps } from 'renovate/dist/logger/remap'

// Directly copied from https://github.com/renovatebot/renovate/blob/39.31.0/lib/workers/global/index.ts
export async function getGlobalConfig(): Promise<RenovateConfig> {
  return await parseConfigs(process.env, process.argv)
}

// Adapted from https://github.com/renovatebot/renovate/blob/39.31.0/lib/workers/global/index.ts#L124-L166
export async function prepareConfig(): Promise<AllConfig> {
  let config: AllConfig
  // read global config from file, env and cli args
  config = await getGlobalConfig()
  if (config?.globalExtends) {
    // resolve global presets immediately
    config = mergeChildConfig(
      await resolveGlobalExtends(config.globalExtends),
      config
    )
  }

  // Set allowedHeaders in case hostRules headers are configured in file config
  GlobalConfig.set({
    allowedHeaders: config.allowedHeaders
  })
  // initialize all submodules
  config = await globalInitialize(config)

  // Set platform, endpoint and allowedHeaders in case local presets are used
  GlobalConfig.set({
    allowedHeaders: config.allowedHeaders,
    platform: config.platform,
    endpoint: config.endpoint
  })

  await validatePresets(config)

  setGlobalLogLevelRemaps(config.logLevelRemap)

  checkEnv()

  await validatePresets(config)

  // validate secrets. Will throw and abort if invalid
  validateConfigSecrets(config)

  return config
}

// Adapted from https://github.com/renovatebot/renovate/blob/39.31.0/lib/workers/repository/index.ts#L50-L116
export async function renovateRepo(
  repoConfig: RenovateConfig
): Promise<ExtractResult> {
  // <modified>
  repoConfig = removeAbilityToDisableRenovateGlobally(repoConfig)
  // </modified>

  let config = GlobalConfig.set(
    applySecretsToConfig(repoConfig, undefined, false)
  )
  config = await initRepo(config)

  // <modified>
  config = removeAbilityToDisableRenovateInLocalRepoConfig(config, repoConfig)
  logger.trace({ config }, 'Final configuration before extracting dependencies')
  logger.debug({ packageRules: config.packageRules }, 'Final packageRules before extracting dependencies')
  logger.debug({ customManagers: config.customManagers }, 'Final customManagers before extracting dependencies')
  // </modified>

  return await extractDependencies(config)
}

// Directly copied from https://github.com/renovatebot/renovate/blob/39.31.0/lib/workers/global/index.ts
function checkEnv(): void {
  const range = pkg.engines.node
  if (process.release?.name !== 'node' || !process.versions?.node) {
    logger.warn(
      { release: process.release, versions: process.versions },
      'Unknown node environment detected.'
    )
  } else if (!semver.satisfies(process.versions?.node, range)) {
    logger.error(
      { versions: process.versions, range },
      'Unsupported node environment detected. Please update your node version.'
    )
  }
}
